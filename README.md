# GitPod template for Quarkus application  

This GitPod template allows to work on Quarkus applications.

Based on [this custom image](https://gitlab.com/jeanphi-baconnais/docker-image-quarkus-gitpod), it allows to have a JDK and Quarkus CLI to develop Quarkus applications ✨.

[![Open in Gitpod](https://gitpod.io/button/open-in-gitpod.svg)](https://gitpod.io/#https://gitlab.com/jeanphi-baconnais/template-gitpod-java-quarkus/-/tree/main/)
